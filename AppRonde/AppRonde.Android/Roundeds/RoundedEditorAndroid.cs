﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppRonde.Droid.Roundeds;
using AppRonde.Roundeds;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EditorRounded), typeof(RoundedEditorAndroid))]
namespace AppRonde.Droid.Roundeds
{
    public class RoundedEditorAndroid : EditorRenderer
    {
        public RoundedEditorAndroid(Context context) : base(context)
        {

        }
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {

                Control.SetBackgroundResource(Resource.Layout.StyleEntry);
                Control.SetPadding(15, Control.PaddingTop, Control.PaddingRight, Control.PaddingBottom);

            }
        }
    }
}