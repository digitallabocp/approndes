﻿using Xamarin.Forms;

namespace AppRonde.Roundeds
{
    public class CustomWebView : WebView
    {
#pragma warning disable CS0618 // Type or member is obsolete
        public static readonly BindableProperty UriProperty = BindableProperty.Create<CustomWebView, string>(p => p.Uri, default(string));
#pragma warning restore CS0618 // Type or member is obsolete

        public string Uri
        {
            get { return (string)GetValue(UriProperty); }
            set { SetValue(UriProperty, value); }
        }
    }
}
