﻿using AppRonde.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppRonde.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Documentations : ContentPage
    {
        List<ModelDocumentation> Files = new List<ModelDocumentation>();
        public Documentations()
        {
            InitializeComponent();


            Files.Add(new ModelDocumentation { ImageUrl = "pdfIcon.png", FileUrl = "Contactsecurite.pdf", Titre = "Contact sécurité" });
            Files.Add(new ModelDocumentation { ImageUrl = "pdfIcon.png", FileUrl = "Bande-dessineeTravauxenHauteur.pdf", Titre = "Bande-dessinée Travaux en Hauteur" });
            Files.Add(new ModelDocumentation { ImageUrl = "pdfIcon.png", FileUrl = "StandardTravailenhauteur.pdf", Titre = "Standard Travail en hauteur" });


            ListFiles.ItemsSource = Files;
            ListFiles.ItemSelected += ListFiles_ItemSelected;

        }
        private async void ListFiles_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as ModelDocumentation;
            if (item != null)
            {
                if (item.FileUrl.Contains(".pdf"))
                {
                    await Navigation.PushAsync(new DetailsDocumentation(item));
                }
                else if (!item.FileUrl.Contains(".pdf") && item.FileUrl != "")
                {
                    Device.OpenUri(new Uri(item.FileUrl));

                }

            }
            ((ListView)sender).SelectedItem = null;
        }
    }
}