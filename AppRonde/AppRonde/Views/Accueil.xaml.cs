﻿using AppRonde.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppRonde.Views
{
    public partial class Accueil : ContentPage
    {
        List<ModelRondes> Rondes = new List<ModelRondes>();
        public Accueil()
        {
            InitializeComponent();
            Rondes.Add(new ModelRondes { Id = 1, DefaultValut = "1", Description = "QUART APRES-MIDI FR07-FR08", Heure = "15:44", MatriculeCreateur = "80679", Nomber = "1/20" });
            Rondes.Add(new ModelRondes { Id = 2, DefaultValut = "5", Description = "QUART APRES-MIDI FR07-FR09", Heure = "08:44", MatriculeCreateur = "80678", Nomber = "1/5" });
            Rondes.Add(new ModelRondes { Id = 3, DefaultValut = "12", Description = "QUART APRES-MIDI FR07-FR10", Heure = "19:44", MatriculeCreateur = "80677", Nomber = "1/19" });
            Rondes.Add(new ModelRondes { Id = 4, DefaultValut = "2", Description = "QUART APRES-MIDI FR07-FR11", Heure = "09:30", MatriculeCreateur = "80676", Nomber = "1/25" });
            Rondes.Add(new ModelRondes { Id = 5, DefaultValut = "9", Description = "QUART APRES-MIDI FR07-FR12", Heure = "13:44", MatriculeCreateur = "80675", Nomber = "1/32" });
            Rondes.Add(new ModelRondes { Id = 6, DefaultValut = "14", Description = "QUART APRES-MIDI FR07-FR13", Heure = "14:44", MatriculeCreateur = "80675", Nomber = "1/4" });
            Rondes.Add(new ModelRondes { Id = 7, DefaultValut = "20", Description = "QUART APRES-MIDI FR07-FR14", Heure = "10:44", MatriculeCreateur = "80674", Nomber = "1/1" });
            Rondes.Add(new ModelRondes { Id = 8, DefaultValut = "7", Description = "QUART APRES-MIDI FR07-FR15", Heure = "12:00", MatriculeCreateur = "80673", Nomber = "1/9" });
            Rondes.Add(new ModelRondes { Id = 9, DefaultValut = "8", Description = "QUART APRES-MIDI FR07-FR16", Heure = "16:30", MatriculeCreateur = "80672", Nomber = "1/10" });
            Rondes.Add(new ModelRondes { Id = 10, DefaultValut = "6", Description = "QUART APRES-MIDI FR07-FR17", Heure = "18:44", MatriculeCreateur = "80671", Nomber = "1/17" });
            Rondes.Add(new ModelRondes { Id = 11, DefaultValut = "24", Description = "QUART APRES-MIDI FR07-FR18", Heure = "17:44", MatriculeCreateur = "80669", Nomber = "1/33" });
            Rondes.Add(new ModelRondes { Id = 12, DefaultValut = "21", Description = "QUART APRES-MIDI FR07-FR19", Heure = "16:44", MatriculeCreateur = "80668", Nomber = "1/22" });
            Rondes.Add(new ModelRondes { Id = 13, DefaultValut = "14", Description = "QUART APRES-MIDI FR07-FR20", Heure = "14:44", MatriculeCreateur = "80667", Nomber = "1/21" });

            ListRondes.ItemsSource = Rondes;
            ListRondes.ItemSelected += ListRondes_ItemSelected;
        }

        private async void ListRondes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var Item = e.SelectedItem as ModelRondes;
            if (Item != null)
            {
                await Navigation.PushAsync(new DetailsRondes(Item));
            }
            ((ListView)sender).SelectedItem = null;
        }
    }
}
