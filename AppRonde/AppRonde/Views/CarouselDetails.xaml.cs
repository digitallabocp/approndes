﻿using AppRonde.Controle;
using AppRonde.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppRonde.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CarouselDetails : ContentPage
    {
        #region Carousel Variables Plus Indicators
        private int _position;
        public int Position { get { return _position; } set { _position = value; OnPropertyChanged(); } }
        #endregion
        List<ModelTacheRound> Taches = new List<ModelTacheRound>();
        List<TypeControle> TypeControles = new List<TypeControle>();
        Stream AttachementFile;
        byte[] Variable;

        public CarouselDetails(ModelRondes model)
        {
            InitializeComponent();

            this.Title = model.Description;

            #region Set Data
            TypeControles.Add(new TypeControle { Id = 1, BackColorType = "#34a50e", ImageChecked = "UnChecked.png", NameType = "Rien à signler" });
            TypeControles.Add(new TypeControle { Id = 2, BackColorType = "#d81c1c", ImageChecked = "UnChecked.png", NameType = "Réglages de chauffe(0,0;-30)" });
            TypeControles.Add(new TypeControle { Id = 3, BackColorType = "#d81c1c", ImageChecked = "UnChecked.png", NameType = "Vannes manuelles fermées" });
            TypeControles.Add(new TypeControle { Id = 4, BackColorType = "#d81c1c", ImageChecked = "UnChecked.png", NameType = "Fuite(s)" });
            TypeControles.Add(new TypeControle { Id = 5, BackColorType = "#d81c1c", ImageChecked = "UnChecked.png", NameType = "Autre défaut" });

            Taches.Add(new ModelTacheRound { NeedDescription = false, HasFiles = true, Id = 1, MatriculeTache = "FBT.FR07", DescriptionTache = "Extracteur TMB", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "1", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#e8e8e8", IsControle = false, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818", ListTypeControle = TypeControles });
            Taches.Add(new ModelTacheRound { NeedDescription = false, HasFiles = false, Id = 2, MatriculeTache = "FBT.FR07", DescriptionTache = "Dosométre minerai", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "2", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#e8e8e8", IsControle = false, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818", ListTypeControle = TypeControles });
            Taches.Add(new ModelTacheRound { NeedDescription = false, HasFiles = true, Id = 3, MatriculeTache = "FBT.FR07", DescriptionTache = "Extracteur TMB-01", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "3", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#1880f7", IsControle = true, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818", ListTypeControle = TypeControles });
            Taches.Add(new ModelTacheRound { NeedDescription = false, HasFiles = false, Id = 4, MatriculeTache = "FBT.FR07", DescriptionTache = "Extracteur TMB-02", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "4", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#e8e8e8", IsControle = false, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818", ListTypeControle = TypeControles });
            Taches.Add(new ModelTacheRound { NeedDescription = false, HasFiles = false, Id = 5, MatriculeTache = "FBT.FR07", DescriptionTache = "Extracteur TMB-03", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "5", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#1880f7", IsControle = true, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818", ListTypeControle = TypeControles });
            Taches.Add(new ModelTacheRound { NeedDescription = false, HasFiles = true, Id = 6, MatriculeTache = "FBT.FR07", DescriptionTache = "Extracteur TMB-04", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "6", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#e8e8e8", IsControle = false, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818", ListTypeControle = TypeControles });
            #endregion

            #region Carousel      
            CarouselRonde.ItemsSource = Taches;
            RondeCarouselIndicators.ItemsSource = Taches;
            CarouselRonde.ItemSelected += CarouselRonde_ItemSelected;
            BindingContext = this;
            #endregion

            GoSave.Clicked += GoSave_Clicked;
        }

        private async void CarouselRonde_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            //ModelTacheRound RItem = new ModelTacheRound();
            //var item = e.SelectedItem as ModelTacheRound;
            //if (item != null)
            //{
            //    for (int i = 0; i < Taches.Count; i++)
            //    {
            //        if (Taches[Position] == item)
            //        {
            //            RItem = Taches[i - 1];
            //        }
            //    }
            //    if (!string.IsNullOrEmpty(RItem.Commentaire) || RItem.ListTypeControle.Where(q => q.ImageChecked == "Checked.png").Any())
            //    {
            //        PopupConfirmation Confirmation = new PopupConfirmation();
            //        await PopupNavigation.Instance.PushAsync(Confirmation, true);
            //    }
            //}
            var item = Taches[Position];
           
            if (!string.IsNullOrEmpty(item.Commentaire) || item.ListTypeControle.Where(q => q.ImageChecked == "Checked.png").Any())
            {
                PopupConfirmation Confirmation = new PopupConfirmation();
                Confirmation.ParamsAcquired += Confirmation_ParamsAcquired;
                await PopupNavigation.Instance.PushAsync(Confirmation, true);
            }
        }

        private async void GoSave_Clicked(object sender, EventArgs e)
        {
            PopupConfirmation Confirmation = new PopupConfirmation();
            Confirmation.ParamsAcquired += Confirmation_ParamsAcquired;
            await PopupNavigation.Instance.PushAsync(Confirmation, true);
        }

        private void Confirmation_ParamsAcquired(object sender, ModelResultConfirmation e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                //if (!e.IsAgree)
                //{
                foreach (var item in TypeControles)
                {
                    item.ImageChecked = "UnChecked.png";
                }
                foreach (var item in Taches)
                {
                    item.Commentaire = "";
                }

                //}
            });
        }

        private void SelectedItem_SousDomaine(object sender, SelectedItemChangedEventArgs e)
        {

            var SelectedItem = sender as CustomDetailsList;
            var item = SelectedItem.SelectedItem as Models.TypeControle;
            if (item != null)
            {


                if (item.ImageChecked == "UnChecked.png")
                {
                    item.ImageChecked = "Checked.png";
                    if (item.NameType == "Rien à signler")
                    {
                        foreach (var type in TypeControles)
                        {
                            if (type.NameType != "Rien à signler")
                                type.ImageChecked = "UnChecked.png";
                        }
                        //foreach (var Tache in Taches)
                        //{                           
                        //        Tache.NeedDescription = false;
                        //}
                    }
                    else
                    {
                        foreach (var type in TypeControles)
                        {
                            if (type.NameType == "Rien à signler")
                                type.ImageChecked = "UnChecked.png";
                        }
                        //foreach (var Tache in Taches)
                        //{
                        //    Tache.NeedDescription = true;
                        //}
                    }
                }
                else
                {
                    item.ImageChecked = "UnChecked.png";
                    //foreach (var Tache in Taches)
                    //{
                    //    Tache.NeedDescription = false;
                    //}
                }


            }
        ((CustomDetailsList)sender).SelectedItem = null;

        }

        private async void Click_GoFiles(object sender, EventArgs e)
        {
            var StackLayout = sender as StackLayout;
            var Item = StackLayout.BindingContext as ModelTacheRound;
            if (Item.HasFiles)
            {
                await Navigation.PushAsync(new Documentations());
            }
        }

        private async void Click_Photo2(object sender, EventArgs e)
        {
            var Image = sender as Image;
            var Item = Image.BindingContext as ModelTacheRound;

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Small,
                MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Front
            });

            if (file == null)
                return;

            AttachementFile = file.GetStream();
            StreamReader reader = new StreamReader(AttachementFile);
            //Item.Image2 = reader.ReadToEnd();
            Item.Image2 = file.Path;


        }

        private async void Click_Photo1(object sender, EventArgs e)
        {
            var Image = sender as Image;
            var Item = Image.BindingContext as ModelTacheRound;
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Small,
                MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Front
            });

            if (file == null)
                return;

            AttachementFile = file.GetStream();
            StreamReader reader = new StreamReader(AttachementFile);
            //Item.Image1 = reader.ReadToEnd();
            Item.Image1 = file.Path;

        }

        private async void Click_Photo3(object sender, EventArgs e)
        {
            var Image = sender as Image;
            var Item = Image.BindingContext as ModelTacheRound;
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Small,
                MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Front
            });

            if (file == null)
                return;

            AttachementFile = file.GetStream();
            StreamReader reader = new StreamReader(AttachementFile);
            //Item.Image3 = reader.ReadToEnd();
            Item.Image3 = file.Path;

        }
    }
}