﻿using AppRonde.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppRonde.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupConfirmation : PopupPage
    {
        public event EventHandler<ModelResultConfirmation> ParamsAcquired;
        ModelResultConfirmation args = new ModelResultConfirmation();
        private bool FiltersSelected = false;
        public PopupConfirmation()
        {
            InitializeComponent();
            GoAddTag.Clicked += GoAddTag_Clicked;
            GoCancel.Clicked += GoCancel_Clicked;

        }

        private async void GoCancel_Clicked(object sender, EventArgs e)
        {
            args = new ModelResultConfirmation()
            {
                IsAgree = false
            };

            await PopupNavigation.Instance.PopAsync();
            FiltersSelected = true;
            OnParamsAcquired();
        }

        private async void GoAddTag_Clicked(object sender, EventArgs e)
        {
            args = new ModelResultConfirmation()
            {
                IsAgree = true
            };

            await PopupNavigation.Instance.PopAsync();
            FiltersSelected = true;
            OnParamsAcquired();
        }
        private void OnParamsAcquired()
        {
            if (FiltersSelected)
            {
                ParamsAcquired?.Invoke(this, args);
            }
        }
    }
}