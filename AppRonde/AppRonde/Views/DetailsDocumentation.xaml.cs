﻿using AppRonde.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppRonde.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetailsDocumentation : ContentPage
	{
		public DetailsDocumentation (ModelDocumentation file)
		{
			InitializeComponent ();

            this.Title = file.Titre;
            WebViewLocal.Uri = file.FileUrl;
        }
	}
}