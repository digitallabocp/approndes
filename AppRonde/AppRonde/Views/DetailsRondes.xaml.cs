﻿using AppRonde.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppRonde.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailsRondes : ContentPage
    {
        List<ModelTacheRound> Taches = new List<ModelTacheRound>();
        ModelRondes ModelItem;
        public DetailsRondes(ModelRondes model)
        {
            InitializeComponent();
            this.Title = model.Description;
            ModelItem = model;

            TitreDescription.Text = model.Nomber + " point de contrôle saisi - " + model.DefaultValut + " défaut";
            TxtHeureDebut.Text = model.Heure;
            TxtCreateur.Text = model.MatriculeCreateur;

            Taches.Add(new ModelTacheRound { Id = 1, MatriculeTache = "FBT.FR07", DescriptionTache = "Extracteur TMB", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "1", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#e8e8e8", IsControle = false, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818" });
            Taches.Add(new ModelTacheRound { Id = 2, MatriculeTache = "FBT.FR07", DescriptionTache = "Dosométre minerai", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "2", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#e8e8e8", IsControle = false, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818" });
            Taches.Add(new ModelTacheRound { Id = 3, MatriculeTache = "FBT.FR07", DescriptionTache = "Extracteur TMB-01", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "3", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#1880f7", IsControle = true, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818" });
            Taches.Add(new ModelTacheRound { Id = 4, MatriculeTache = "FBT.FR07", DescriptionTache = "Extracteur TMB-02", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "4", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#e8e8e8", IsControle = false, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818" });
            Taches.Add(new ModelTacheRound { Id = 5, MatriculeTache = "FBT.FR07", DescriptionTache = "Extracteur TMB-03", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "5", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#1880f7", IsControle = true, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818" });
            Taches.Add(new ModelTacheRound { Id = 6, MatriculeTache = "FBT.FR07", DescriptionTache = "Extracteur TMB-04", Text1Tache = "Etat du plancher ,des grilles de protection, de la chute sur dosmétre de fonctionnement des canons de décolmatage.", RondeId = 1, NumberTache = "6", TypeTache = "APM.", Text2Tache = "En cas de fuite, préciser la nature (eau, air, fioul, poussiéres, charbon,...)", StatutTache = "#e8e8e8", IsControle = false, StatutControle = "Autre défaut", StatutBackColorControle = "#D31818" });

            ListTachesRonde.ItemsSource = Taches;
            ListTachesRonde.ItemSelected += ListTachesRonde_ItemSelected;
        }

        private async void ListTachesRonde_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            await Navigation.PushAsync(new CarouselDetails(ModelItem));
        }
    }
}