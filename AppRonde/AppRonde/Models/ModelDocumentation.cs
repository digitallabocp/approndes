﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppRonde.Models
{
    public class ModelDocumentation
    {
        public long Id { get; set; }
        public string Titre { get; set; }
        public string ImageUrl { get; set; }
        public string FileUrl { get; set; }
    }
}
