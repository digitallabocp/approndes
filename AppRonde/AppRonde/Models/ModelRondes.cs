﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppRonde.Models
{
    public class ModelRondes
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public string DefaultValut { get; set; }
        public string MatriculeCreateur { get; set; }
        public string Nomber { get; set; }
        public string Heure { get; set; }
    }
}
