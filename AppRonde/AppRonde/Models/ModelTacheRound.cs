﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppRonde.Models
{
    public class ModelTacheRound : Xamarin.Forms.BindableObject
    {
        public long Id { get; set; }
        public long RondeId { get; set; }
        public string TypeTache { get; set; }
        public string NumberTache { get; set; }
        public string MatriculeTache { get; set; }
        public string DescriptionTache { get; set; }
        public string Text1Tache { get; set; }
        public string Text2Tache { get; set; }
        public string StatutTache { get; set; }
        public string StatutControle { get; set; }
        public string StatutBackColorControle { get; set; }
        public bool IsControle { get; set; }
        public bool HasFiles { get; set; }
        public List<TypeControle> ListTypeControle { get; set; }
        public bool needDescription;
        public bool NeedDescription
        {
            get { return needDescription; }
            set { needDescription = value; OnPropertyChanged(); }
        }
        public string image1;
        public string Image1
        {
            get { return image1; }
            set { image1 = value; OnPropertyChanged(); }
        }
        public string image2;
        public string Image2
        {
            get { return image1; }
            set { image1 = value; OnPropertyChanged(); }
        }
        public string image3;
        public string Image3
        {
            get { return image1; }
            set { image1 = value; OnPropertyChanged(); }
        }
        public string commentaire;
        public string Commentaire
        {
            get { return commentaire; }
            set { commentaire = value; OnPropertyChanged(); }
        }
       
    }

    public class TypeControle : Xamarin.Forms.BindableObject
    {
        public long Id { get; set; }
        public string NameType { get; set; }
        public string BackColorType { get; set; }
        public string imageChecked;
        public string ImageChecked
        {
            get { return imageChecked; }
            set { imageChecked = value; OnPropertyChanged(); }
        }



    }
}
